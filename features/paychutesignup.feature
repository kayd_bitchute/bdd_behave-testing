Feature: Paychute Homepage sign up
  Background: common steps
    Given I launch browser
    When I open the paychute homepage
    And Enter email "john@gmail.com" and password "1234" and password_confirm "1234"

  Scenario: Sign up with invalid params
      When Click on sign up Button
      Then User must failed to open News feeds

  Scenario: Sign up to Paychute
      When Click on Accept the terms of services
      And Click on sign up button
      And Click on authorize button
      Then User must successfully open News feeds

    Scenario: Sign up with duplicate params
      When Click on Accept the terms of services
      And Click on sign up button
      And Click on authorize button
      Then User must failed to open News feeds

