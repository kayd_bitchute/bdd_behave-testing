import time

from behave import *
from selenium.webdriver.common.by import By
from selenium.common import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# time loading second
delay = int(5)


@when('I click sign in with bitchute')
def step_impl(context):
    signin_bitchute = context.driver.find_element(By.LINK_TEXT, 'Sign in with Bitchute')
    signin_bitchute.click()


@then('User must successfully open Authorize Paychute page')
def step_impl(context):
    current_url = context.driver.current_url
    if '9000' in current_url:
        context.driver.close()
        assert True, 'Test Passed'

    else:
        context.driver.close()
        assert False, 'Test Failed'


@when('I click log in button')
def step_impl(context):
    login_btn = context.driver.find_element(By.CSS_SELECTOR, "a[href='/login']")
    login_btn.click()


@then('User must successfully open Login page')
def step_impl(context):
    current_url = context.driver.current_url
    if '/login' in current_url:
        context.driver.close()
        assert True, 'Test Passed'

    else:
        context.driver.close()
        assert False, 'Test Failed'
    '''
    try:
        WebDriverWait(context.driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'Sign In')]")))
        context.driver.close()
        assert True, 'Test Passed'
    except NoSuchElementException:
        context.driver.close()
        assert False, 'Test Failed'
    '''


@when('I click terms & condition link')
def step_impl(context):
    terms_btn = context.driver.find_element(By.LINK_TEXT, 'Terms & Conditions')
    terms_btn.click()


@then('User must successfully open Terms & Condition')
def step_impl(context):
    try:
        WebDriverWait(context.driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'Terms & Conditions')]")))
        context.driver.close()
        assert True, 'Test Passed'
    except NoSuchElementException:
        context.driver.close()
        assert False, 'Test Failed'


@when('I click privacy policy link')
def step_impl(context):
    privacy_btn = context.driver.find_element(By.LINK_TEXT, 'Privacy Policy')
    privacy_btn.click()


@then('User must successfully open Privacy Policy')
def step_impl(context):
    try:
        WebDriverWait(context.driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'Privacy Policy')]")))
        context.driver.close()
        assert True, 'Test Passed'
    except NoSuchElementException:
        context.driver.close()
        assert False, 'Test Failed'


@when('I click community guidelines link')
def step_impl(context):
    guideline_btn = context.driver.find_element(By.LINK_TEXT, 'Community Guidelines')
    guideline_btn.click()


@then('User must successfully open Community Guidelines')
def step_impl(context):
    try:
        WebDriverWait(context.driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'Community Guidelines')]")))
        context.driver.close()
        assert True, 'Test Passed'
    except NoSuchElementException:
        context.driver.close()
        assert False, 'Test Failed'


@when('I click contact us link')
def step_impl(context):
    privacy_btn = context.driver.find_element(By.LINK_TEXT, 'Contact Us')
    privacy_btn.click()


@then('User must successfully open Contact Details')
def step_impl(context):
    try:
        WebDriverWait(context.driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'Contact Details')]")))
        context.driver.close()
        assert True, 'Test Passed'
    except NoSuchElementException:
        context.driver.close()
        assert False, 'Test Failed'


@when('I click cookie policy link')
def step_impl(context):
    cookie_btn = context.driver.find_element(By.LINK_TEXT, 'Cookie Policy')
    cookie_btn.click()


@then('User must successfully open Cookie Policy')
def step_impl(context):
    try:
        WebDriverWait(context.driver, delay).until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'Cookie Policy')]")))
        context.driver.close()
        assert True, 'Test Passed'
    except NoSuchElementException:
        context.driver.close()
        assert False, 'Test Failed'
