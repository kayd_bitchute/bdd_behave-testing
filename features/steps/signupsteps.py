import time
from behave import *
from selenium import webdriver
from selenium.common import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.common.by import By
from subprocess import getoutput
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

# config
options = Options()
options.binary_location = getoutput("find /snap/firefox -name firefox").split("\n")[-1]

# time loading second
delay = int(5)


@given('I launch browser')
def step_impl(context):
    context.driver = webdriver.Firefox(
        service=Service(executable_path=getoutput("find /snap/firefox -name geckodriver").split(
            "\n")[-1]), options=options)


@when('I open the paychute homepage')
def step_impl(context):
    context.driver.get('http://localhost:8080')


@when('Enter email "{em}" and password "{pwd}" and password_confirm "{pwd_confirm}"')
def step_impl(context, em, pwd, pwd_confirm):
    email = WebDriverWait(context.driver, delay).until(EC.presence_of_element_located((By.XPATH, "//input[@placeholder='Email']")))
    password = context.driver.find_element(By.XPATH, "//input[@placeholder='Password']")
    password_confirm = context.driver.find_element(By.XPATH, "//input[@placeholder='Confirm password']")
    time.sleep(delay)
    email.send_keys(em)
    password.send_keys(pwd)
    password_confirm.send_keys(pwd_confirm)


@when('Click on Accept the terms of services')
def step_impl(context):
    policy = context.driver.find_element(By.CLASS_NAME, "q-checkbox__truthy")
    action = ActionChains(context.driver)
    action.click(on_element=policy)
    action.perform()


@when('Click on sign up button')
def step_impl(context):
    submit = context.driver.find_element(By.XPATH, "//button[@type='submit']")
    submit.click()


@when('Click on authorize button')
def step_impl(context):
    time.sleep(15)
    authorize = context.driver.find_element(By.NAME, 'allow')
    authorize.click()


@then('User must successfully open News feeds')
def step_impl(context):
    time.sleep(delay)
    try:
        feeds = context.driver.find_element(By.CSS_SELECTOR, "div:contains('News feeds')")
        context.driver.close()
        assert True, 'Test Passed'
    except NoSuchElementException:
        context.driver.close()
        assert False, 'Test Failed'


@then('User must failed to open News feeds')
def step_impl(context):
    time.sleep(15)
    current_path = context.driver.current_url

    if '9000' in current_path:
        context.driver.close()
        assert False, 'Test Failed'

    else:
        assert True, 'Test Passed'
